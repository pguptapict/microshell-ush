/*
* Micro-Shell. Supports most of the C shell commands except job commands.
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#include "parse.h"
#include <signal.h>
#include <unistd.h>
#include <errno.h>
#include <signal.h>
#include <sys/resource.h>


typedef struct Command Command;
struct Command{
	Cmd cmd;
	int readEnd;
	int writeEnd;
	struct Command *next;
};

int processCommands(Cmd c,char *argv[], char *envp[]);
int commandToExecute(Cmd c);
void executeCommands(Pipe p,char *envp[]);
int countCommands(Cmd c);
int executeSingleCommand(Command *c, char *envp[]);
int executePipedCommands(Command *leftCmd, Command *rightCmd,char *envp[]);
int performInRedirection(Cmd c);
int performOutRedirection(Cmd c);
Command* prepareCommands(Cmd c);
int builtIn(char *str);
void initializeShell(char* file,char *envp[]);
int interactiveMode(char *envp[]);
int nonInteractiveMode(char *envp[]);
void addSignalHandlers();
static Cmd newCmd(char *cmd);
static char *mkWord(char *s);
int niceness = -9999;
int current_niceness;
int toContinue;


/*
* Function to handle ctrl^c signal.
*
* Input : Signal number.
*
* Returns : void.
*/

void handleSIGINT(int s){
	toContinue = 1;
        printf("\n");
}


/*
* Function to handle ctrl^z signal.
*
* Input : Signal number.
*
* Returns : void.
*/

void handleSIGSTP(int s){
	   toContinue = 1;
           printf("\n");
}


/*
* Main Function.
*/

int main(int argc, char *argv[], char *envp[])
{
	
	int status,no_of_commands=0,no_of_pipes=1;
	char host[] = "ush";
	char *current_dir;
	Pipe p,tempP;
	int commandChains=0;	

	Cmd c;
	current_dir=(char *)malloc(500*sizeof(char));
	addSignalHandlers();
	initializeShell(".ushrc",envp);
	while(1){
		fflush(stdin);
		getcwd(current_dir,500);
		printf("%s:~%s%% ", host,current_dir);
		fflush(stdout);
    		p = parse();
		if(p==NULL)
			continue;
		if(strcmp(p->head->args[0],"end")==0){
			if(toContinue == 1){
				toContinue = 0;
				continue;
			}else
				break;
		}	
		executeCommands(p,envp);
	}
	return 0;
}


/*
* Function to initialize shell with .ushrc file. This .ushrc is a configuration file that is to be loaded and executed when
* micro-shell begins its execution.
*
* Input : fileName, envp.
*
* Returns : void.
*/

void initializeShell(char *fileName,char *envp[]){
	int file;
	Pipe p;
	int current_in = dup(STDIN_FILENO);
	int current_out = dup(STDOUT_FILENO);
	file = open(fileName,O_RDWR,0777);
	if(file != -1){
		dup2(file,STDIN_FILENO);	
		while(1){
			p = parse();
			if(p != NULL && strcmp(p->head->args[0],"end")!=0)
				executeCommands(p,envp);
			else
		   		break;	
		}
	
		dup2(current_in,STDIN_FILENO);
		dup2(current_out,STDOUT_FILENO);
		close(file);
	}
}


/*
* Function to handle signals ctrl^c and ctrl^z.
*
* Input : void.
*
* Returns : void.
*/

void addSignalHandlers(){
	struct sigaction sigIntHandler;
	struct sigaction sigSTPHandler;
   	sigIntHandler.sa_handler = handleSIGINT;
   	sigemptyset(&sigIntHandler.sa_mask);
   	sigIntHandler.sa_flags = 0;
   	sigaction(SIGINT, &sigIntHandler, NULL);
	//signal(SIGQUIT, SIG_IGN);

	sigSTPHandler.sa_handler = handleSIGSTP;
   	sigemptyset(&sigSTPHandler.sa_mask);
   	sigSTPHandler.sa_flags = 0;
   	sigaction(SIGTSTP, &sigSTPHandler, NULL);
	//signal(SIGTSTP, SIG_IGN);
}


/*
* Function to execute a Pipe.
*
* Input : Pipe,envp
*
* Returns : void.
*/

void executeCommands(Pipe p, char *envp[]){
	int countCommand;
	Command *c = NULL;
	while(p!=NULL){
		countCommand = countCommands(p->head);
		c = prepareCommands(p->head);
		if(countCommand == 1)
			executeSingleCommand(c,envp);
		else
			executePipedCommands(c,c->next,envp);
	p = p->next;
	}
}


/*
* Function to prepare a new structure. This new structure will contain a Cmd structure along with 
* in and out pipe descriptor information.
*
* Input : Cmd
*
* Returns : Command chain.
*/

Command* prepareCommands(Cmd c){
	Command* Commandhead = NULL;
	Command* Commandtail = NULL;
	while(c!=NULL){
		Command *command = (Command *)malloc(sizeof(Command));
		command->cmd = c;
		command->readEnd = fileno(stdin);
		command->writeEnd = fileno(stdout);
		command->next = NULL;
		if(Commandhead == NULL){
			Commandhead = command;
			Commandtail = command;
		}
		else {
			Commandtail->next = command;
			Commandtail = command;
		} 
		c = c->next;
	}
	return Commandhead;
}

/*
* Function to execute an entire command chain. This function breaks Command chain into individual commands and call 
* excuteSingleCommand() for each command. This function also creates a pipe between the 2 commands.
*
* Input : Cmd on the left of Pipe. Cmd on right of Pipe.
*
* Returns : exitStatus.
*/

int executePipedCommands(Command *leftCommand, Command *rightCommand,char *envp[]){
	
	int pipes[2];
	int pidLeft,pidRight,status, childstatus;;
	int i;
	
	if(rightCommand == NULL){
	        // if this is the last command in the pipeline, run it and return
	        return executeSingleCommand(leftCommand,envp);
	    }

	pipe(pipes);
	
    	pidRight = fork(); // fork once to execute pipeline on right
    
        if(pidRight < 0){
        	fprintf(stderr, "Error! Could not fork process for command '%s'.\n", rightCommand->cmd->args[0]);
        	exit(1);
    	}else if(pidRight == 0){
        	close(pipes[1]);
        	dup2(pipes[0],rightCommand->readEnd);
        	exit(executePipedCommands(rightCommand, rightCommand->next,envp));
    	}
    	else{
        	pidLeft = fork(); // fork again to execute left command
        
       		if(pidLeft < 0){
            		fprintf(stderr, "Error! Could not fork process for command '%s'.\n", leftCommand->cmd->args[0]);
            		exit(1);
        	}
        	else if(pidLeft == 0){
        		close(pipes[0]);
        		dup2(pipes[1],leftCommand->writeEnd);
            		exit(executeSingleCommand(leftCommand,envp));
       		}
        	else{
            		close(pipes[0]);
            		close(pipes[1]);
            
            		waitpid(pidRight, &childstatus, 0);
            		childstatus = WEXITSTATUS(childstatus);
            
            		waitpid(pidLeft, &status, 0);
            		status = WEXITSTATUS(status);
            		status += WEXITSTATUS(childstatus);
        	}
   	 }
    return status;
}




/*
* Function to execute a single command in a Pipe.
*
* Input : Cmd,envp
*
* Returns : exitStatus.
*/

int executeSingleCommand(Command *command,char *envp[]){
	int statusInSuccess;
	if(commandToExecute(command->cmd)==1 || commandToExecute(command->cmd)==6 || commandToExecute(command->cmd)==4 || 				commandToExecute(command->cmd)==5 || commandToExecute(command->cmd)==9){
		processCommands(command->cmd,command->cmd->args,envp);
		return;	
	}

	int pid = fork();
	int status;
	
	if(pid < 0){
	        fprintf(stderr, "Error! Could not fork process for command '%s'.\n", command->cmd->args[0]);
	        exit(1);
	}else if(pid == 0){
		if(niceness != -9999){
			if(nice(niceness)==-1){
			 	if(errno == EPERM){
					fflush(stdout);
					niceness = -9999;
					printf("Permission Denied: Cannot set niceness.\n");
					//return;
				}
			}
			niceness = -9999;
		}
		if(command->cmd->in != Tnil)
			statusInSuccess = performInRedirection(command->cmd);
		if(command->cmd->out != Tnil && command->cmd->out != Tpipe && command->cmd->out != TpipeErr)
			performOutRedirection(command->cmd);

		if(!statusInSuccess)
			exit(1);
		dup2(command->readEnd, fileno(stdin));
        	dup2(command->writeEnd, fileno(stdout));
		if(command->cmd->out == TpipeErr)
			dup2(command->writeEnd, fileno(stderr));
		processCommands(command->cmd,command->cmd->args, envp);
		exit(1);
	}else{
		waitpid(pid, &status, 0);
		if(command->readEnd != fileno(stdin)){
            		close(command->readEnd);
       		}
        	if(command->writeEnd != fileno(stdout)){
            		close(command->writeEnd);
        	}
        
        	status = WEXITSTATUS(status);
		
    	}
    
    	return status;
	
}



/*
* Function to count total commands in a Pipe.
*
* Input : Cmd
*
* Returns : Command count.
*/

int countCommands(Cmd c){
	int count=0;
	while(c!=NULL){
		c = c->next;
		count++;
	}
	return count;
}


/*
* Function responsible for processing commands. This function takes a Cmd and runs it. Calls exec if not built in command.
*
* Input : Cmd, argv, envp
*
* Returns : Nothing
*/

int processCommands(Cmd c,char *argv[], char *envp[]){
	int which = PRIO_PROCESS,countCommand;
	id_t pid;
	Command *command; Pipe p; 
	int current_in = dup(0);
	int current_out = dup(1);
	char *path,*current_dir, *pch;
	void (*sig)(int);
	int i,errNo,isNumber;
	int file,count=2;
	struct stat st;
	char *endptr;
	char buf[256],**environment;
	
	extern char **environ;
		switch(commandToExecute(c)){
			case 1:	//cd command
				path = c->args[1];
				if(path == NULL)
					path = getenv("HOME");
				else if((strcmp(path,"")==0 || strcmp(path,"~")==0))
					path = getenv("HOME");
				if(chdir(path)!=0)
					printf("Invalid Path\n");	
				break;
			
			case 2: //pwd command
				current_dir=(char *)malloc(500*sizeof(char));
				getcwd(current_dir,500);
				printf("%s\n",current_dir);
				free(current_dir);	
				break;

			case 3: //echo command
				if (c->nargs > 1 ) {
     					for (i = 1; c->args[i] != NULL; i++)
						printf("%s ", c->args[i]);
      					printf("\n");
    				}
				break;
			case 4: //setenv command 
				if(c->nargs > 3)
					printf("setenv: Too many arguments\n");
			
				if(c->nargs == 1){
					i =0;
					environment = environ;
					if(c->out != Tnil && c->out != Tpipe && c->out != TpipeErr){
						performOutRedirection(c);
						while(environment[i] != NULL)
							printf("%s\n",environment[i++]);
						dup2(current_out,STDOUT_FILENO);
					}else{
						while(environment[i] != NULL)
							printf("%s\n",environment[i++]);
					}
					break;
				}
				setenv(c->args[1],c->args[2],1);
				break;
			case 5: //unsetenv command 
				if(c->nargs > 2)
					fprintf(stderr,"setenv: Too many arguments\n");	
				if(unsetenv(c->args[1])==-1)
					printf("Error\n");
				path = getenv(c->args[1]);
				break;
			
			case 6: // logout
				kill(getpid(), SIGKILL);
				break;

			case 7: //clear screen
				printf("%c[2J",27);
				printf("%c[0;0H",27);
				break;

			case 8: // Where command
				if(builtIn(c->args[1])){
					printf("%s is a built in\n",c->args[1]);
				break;				
				}

				path = getenv("PATH");
				pch = strtok (path,":");
 				while (pch != NULL)
  				{
    					snprintf(buf, sizeof buf, "%s/%s", pch, c->args[1]);
					if(stat(buf,&st) == 0){
        					printf("%s\n",buf);
					}
    					pch = strtok (NULL, ":");
  				}
				break;
	
			case 9: //nice command
				pid = getpid();
				current_niceness = getpriority(which, pid);
				count = 0;
				isNumber = 0;
				if(c->nargs== 1){
					printf("%d\n",current_niceness);
					c->nargs = c->nargs - 1;
					break;
				}
				if(c->nargs>1){
					niceness = strtol(c->args[1],&endptr,10);
					if(*endptr=='\0')
						isNumber = 1;

					if(c->nargs == 2 && isNumber){
						if(nice(niceness)==-1){
							if(errno == EPERM){
								fflush(stdout);
								niceness = -9999;
								printf("Permission Denied: Cannot set niceness.\n");
								
							}
						}
					}else
					if(c->nargs >= 2 && isNumber){
						//niceness = 4;
						if(nice(niceness)==-1){
							if(errno == EPERM){
								fflush(stdout);
								niceness = -9999;
								printf("Permission Denied: Cannot set niceness.\n");
								
							}
						}
						count = 2;
						while(c->args[count]!=NULL){
							c->args[count-2] = c->args[count];
							count++;
						}
						c->nargs = c->nargs - 2;
						c->args[c->nargs] = NULL;
						countCommand = countCommands(c);
						command = prepareCommands(c);
						if(countCommand == 1)
							executeSingleCommand(command,envp);
						else
							executePipedCommands(command,command->next,envp);
						break;

					}else
					if(c->nargs >= 2 && !isNumber){
						niceness = 4;
						count = 1;
						while(c->args[count]!=NULL){
							c->args[count-1] = c->args[count];
							count++;
						}
						c->nargs = c->nargs - 1;
						c->args[c->nargs] = NULL;
						countCommand = countCommands(c);
						command = prepareCommands(c);
						if(countCommand == 1)
							executeSingleCommand(command,envp);
						else
							executePipedCommands(command,command->next,envp);
						break;
					}

				}	
				break;
			case 11: // other executable
				errNo = execvp(c->args[0], argv);
				if(errno == EACCES)
					printf("Permission Denied!\n");
				else if(errNo == -1)
					printf("%s: Command not Found\n",c->args[0]);
				break;
		}

}



/*
* Function to find the command mapping.
*
* Input : Cmd
*
* Returns : Command code
*/

int commandToExecute(Cmd c){
	if(strcmp(c->args[0],"cd")==0)
		return 1;
	else if(strcmp(c->args[0],"pwd")==0)
		return 2;
	else if(strcmp(c->args[0],"echo")==0)
		return 3;
	else if(strcmp(c->args[0],"setenv")==0)
		return 4;
	else if(strcmp(c->args[0],"unsetenv")==0)
		return 5;
	else if(strcmp(c->args[0],"logout")==0)
		return 6;
	else if(strcmp(c->args[0],"clear")==0)
		return 7;
	else if(strcmp(c->args[0],"where")==0)
		return 8;
	else if(strcmp(c->args[0],"nice")==0)
		return 9;
	
	else
		return 11;
	
}

int commandToExecute1(char *str){
	if(strcmp(str,"cd")==0)
		return 1;
	else if(strcmp(str,"pwd")==0)
		return 2;
	else if(strcmp(str,"echo")==0)
		return 3;
	else if(strcmp(str,"setenv")==0)
		return 4;
	else if(strcmp(str,"unsetenv")==0)
		return 5;
	else if(strcmp(str,"logout")==0)
		return 6;
	else if(strcmp(str,"clear")==0)
		return 7;
	else if(strcmp(str,"where")==0)
		return 8;
	else if(strcmp(str,"nice")==0)
		return 9;
	
	else
		return 11;
	
}


/*
* Function to check whether Command is a built-in or not.
*
* Input : str
*
* Returns : If builtIn-->1 ;  If not builtIn--> 0 
*/

int builtIn(char *str){
	if(strcmp(str,"cd")==0 || strcmp(str,"pwd")==0 || strcmp(str,"echo")==0 || strcmp(str,"setenv")==0 || strcmp(str,"unsetenv")==0 || 			strcmp(str,"logout")==0 || strcmp(str,"clear")==0 || strcmp(str,"where")==0)
		return 1;
	return 0;
}



/*
* Function to perform In redirection.
*
* Input : Cmd
*
* Returns : Success --> 1 ; Failure --> 0
*/

int performInRedirection(Cmd c){
	int file;
	if ( c->in == Tin ){
     		if((file = open(c->infile,O_RDWR,0444))==-1){
			if(errno == EACCES){
				printf("%s: Permission Denied.\n",c->infile);
				return 0;
			}
			printf("%s: File not found.\n",c->infile);
			return 0;
		}
		dup2(file,0);	
		close(file);
	}
	return 1;
}


/*
* Function to perform Output redirection.
*
* Input : Cmd
*
* Returns : Nothing
*/
int performOutRedirection(Cmd c){
	int file;
	switch ( c->out ) {
 	case Tout:
		file = open(c->outfile,O_CREAT|O_RDWR|O_TRUNC,0777);
		if(file==-1)
			printf(" Error in opening file\n");
		dup2(file,1);
		break;
	
	case Tapp:
		file = open(c->outfile,O_CREAT|O_RDWR|O_APPEND,0777);
		if(file==-1)
			printf(" Error in opening file\n");
		dup2(file,1);
		break;

	case ToutErr:
		file = open(c->outfile,O_CREAT|O_RDWR|O_TRUNC,0777);
		if(file==-1)
			printf(" Error in opening file\n");
		dup2(file,1);
		dup2(file,2);
		break;
	
	case TappErr:
		file = open(c->outfile,O_CREAT|O_RDWR|O_APPEND,0777);
		if(file==-1)
			printf(" Error in opening file\n");
		dup2(file,1);
		dup2(file,2);
		break;
	}
	close(file);
}


